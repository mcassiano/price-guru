# PriceGuru

#### An Android app to find the most affordable ride.

[![CircleCI](https://circleci.com/bb/mcassiano/price-guru.svg?style=shield)](https://circleci.com/bb/mcassiano/price-guru/tree/master)

## Key Features

* Comparison among all available options from where you are
* Beautiful and engaging user interface

## Architecture Overview

TODO: how to build and architecture overview.

## Download

TODO: link to latest stable release.

## Credits

This software uses code from several open source packages.

TODO: open source attribution.

## License

MIT



> Webiste [cassiano.me](https://cassiano.me)  
> GitHub [@mcassiano](https://github.com/mcassiano)  
> Twitter [@mcassiano](https://twitter.com/mcassiano)
