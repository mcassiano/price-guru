package me.cassiano.estimate.widget.estimates;

import io.reactivex.Observable;
import me.cassiano.estimate.data.presentation.model.PriceEstimatesViewState;

public interface PriceEstimatesView {

    Observable<Boolean> closeEstimatesIntent();

    void render(PriceEstimatesViewState state);
}
