package me.cassiano.estimate.widget.estimates;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.Observable;
import io.supercharge.shimmerlayout.ShimmerLayout;
import me.cassiano.estimate.R;
import me.cassiano.estimate.data.presentation.model.PriceEstimatesViewState;
import me.cassiano.estimate.estimate.EstimateListAdapter;
import me.cassiano.estimate.utils.GridSpacingItemDecoration;

import static android.support.v4.content.ContextCompat.getColor;

public class PriceEstimates extends ConstraintLayout implements PriceEstimatesView {

    private ShimmerLayout loadingView;
    private TextView title;
    private ImageButton closeButton;
    private TextView noCoverage;
    private RecyclerView estimateList;
    private EstimateListAdapter adapter;

    public PriceEstimates(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.view_price_estimates, this);
        setMinHeight(getResources().getDimensionPixelSize(
                R.dimen.height_price_estimates));
        setElevation(18);
        setBackgroundColor(getColor(getContext(), R.color.white));
        bindViews();
    }

    @Override
    public Observable<Boolean> closeEstimatesIntent() {
        return RxView.clicks(closeButton).map(it -> true);
    }

    @Override
    public void render(PriceEstimatesViewState state) {

        if (state.loadingPrices()) {
            estimateList.setVisibility(GONE);
            loadingView.setVisibility(VISIBLE);
            title.setVisibility(GONE);
            noCoverage.setVisibility(GONE);
        } else if (state.estimates() != null) {
            loadingView.setVisibility(GONE);

            if (state.estimates().isWithinAreaOfOperation()) {
                noCoverage.setVisibility(GONE);
                title.setVisibility(VISIBLE);
                estimateList.setVisibility(VISIBLE);
                adapter.setEstimates(state.estimates());
            } else {
                noCoverage.setVisibility(VISIBLE);
                title.setVisibility(GONE);
                estimateList.setVisibility(GONE);
            }
        }
    }

    private void bindViews() {
        loadingView = findViewById(R.id.dummy_content);
        closeButton = findViewById(R.id.close_button);
        estimateList = findViewById(R.id.estimate_list);
        noCoverage = findViewById(R.id.no_coverage_image);
        title = findViewById(R.id.view_title);
        adapter = new EstimateListAdapter();
        estimateList.setLayoutManager(new GridLayoutManager(getContext(), 3));
        estimateList.setAdapter(adapter);

        int spacingPx = getResources()
                .getDimensionPixelSize(R.dimen.spacing_grid_price_estimates);
        estimateList.addItemDecoration(new GridSpacingItemDecoration(3,
                spacingPx, true, 0));
    }
}
