package me.cassiano.estimate.widget.map;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CustomCap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import me.cassiano.estimate.R;
import me.cassiano.estimate.data.presentation.model.MapViewState;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static android.support.v4.content.ContextCompat.getColor;
import static com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds;
import static com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom;
import static com.google.android.gms.maps.model.BitmapDescriptorFactory.fromBitmap;
import static me.cassiano.estimate.maps.MapStyle.ROUTE_XL;
import static me.cassiano.estimate.utils.Drawables.getBitmap;

public class GuruMapView extends MapView implements MapViewContract, OnMapReadyCallback {

    PublishSubject<Boolean> clickedMyLocation = PublishSubject.create();
    private Marker markerFrom;
    private Marker markerTo;
    private GoogleMap map;
    private Polyline polyline;
    private Polyline animationPolyline;
    private RouteAnimator animator;

    public GuruMapView(Context context) {
        super(context);
    }

    public GuruMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GuruMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void onCreateCalled() {
        getMapAsync(this);
    }

    @Override
    public Observable<Boolean> clickedOnMyLocation() {
        return clickedMyLocation;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        this.map.setMapStyle(ROUTE_XL);

        shouldShowMyLocation(true);

        MarkerOptions from = new MarkerOptions()
                .alpha(0)
                .icon(fromBitmap(getBitmap(getContext(), R.drawable.ic_origin)))
                .flat(true)
                .position(new LatLng(0, 0));

        MarkerOptions to = new MarkerOptions()
                .alpha(0)
                .icon(fromBitmap(getBitmap(getContext(), R.drawable.ic_destination)))
                .flat(true)
                .position(new LatLng(0, 0));

        this.markerFrom = map.addMarker(from);
        this.markerTo = map.addMarker(to);
    }

    @Override
    public void render(MapViewState state) {

        shouldShowMyLocation(state.from() == null || state.to() == null);
        if (animator != null) animator.stop();

        if (state.loadingDirections()) {
            markerFrom.setAlpha(1);
            markerFrom.setPosition(state.from().getLatLng());
            markerTo.setAlpha(1);
            markerTo.setPosition(state.to().getLatLng());
            LatLngBounds bounds = LatLngBounds.builder()
                    .include(state.from().getLatLng())
                    .include(state.to().getLatLng())
                    .build();
            if (polyline != null) polyline.remove();
            if (animationPolyline != null) animationPolyline.remove();
            map.moveCamera(newLatLngBounds(bounds, 50));
            return;
        }

        LatLng latLng = null;

        if (state.from() != null) {
            latLng = state.from().getLatLng();
            markerFrom.setPosition(state.from().getLatLng());
        }
        if (state.to() != null) {
            latLng = state.to().getLatLng();
            markerTo.setPosition(state.to().getLatLng());
        }

        markerFrom.setAlpha(state.from() != null && state.directions() == null ? 1 : 0);
        markerTo.setAlpha(state.to() != null && state.directions() == null ? 1 : 0);

        if (polyline != null) polyline.remove();
        if (animationPolyline != null) animationPolyline.remove();

        if (state.from() != null && state.to() != null) {
            if (state.directions() != null &&
                    !state.directions().isEmpty()) {
                renderPolyline(state.directions());
                startPolylineAnimation(state.directions());
            }
        } else if (latLng != null) {
            map.moveCamera(newLatLngZoom(latLng, 17));
        }

    }

    public void setMapBottomOffset(int bottom) {
        if (this.map != null)
            this.map.setPadding(0, 40, 0, bottom);
    }

    private void renderPolyline(List<LatLng> decodedPath) {
        if (decodedPath.size() == 0) return;
        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        for (LatLng latLng : decodedPath) bounds.include(latLng);
        polyline = map.addPolyline(new PolylineOptions()
                .color(getColor(getContext(), R.color.colorPrimary))
                .endCap(new CustomCap(fromBitmap(getBitmap(getContext(), R.drawable.ic_destination))))
                .width(12)
                .jointType(JointType.ROUND)
                .addAll(decodedPath));
        map.animateCamera(newLatLngBounds(bounds.build(), 100));
    }

    private void startPolylineAnimation(List<LatLng> points) {

        PolylineOptions options = new PolylineOptions()
                .color(getColor(getContext(), R.color.colorPrimaryDark))
                .startCap(new CustomCap(fromBitmap(getBitmap(getContext(), R.drawable.ic_origin))))
                .jointType(JointType.ROUND)
                .width(12);

        animationPolyline = map.addPolyline(options);
        animator = new RouteAnimator(points, interpolatedPoints ->
                animationPolyline.setPoints(interpolatedPoints));
        animator.start();
    }

    private void shouldShowMyLocation(boolean enable) {
        if (checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            this.map.setMyLocationEnabled(enable);
            if (enable)
                this.map.setOnMyLocationButtonClickListener(() -> {
                    clickedMyLocation.onNext(true);
                    return true;
                });
            else this.map.setOnMyLocationButtonClickListener(null);
        }
    }
}
