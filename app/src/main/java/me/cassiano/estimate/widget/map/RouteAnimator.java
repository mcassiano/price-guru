package me.cassiano.estimate.widget.map;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;
import java.util.List;

public class RouteAnimator {

    private final List<LatLng> interpolatedPoints;
    private final List<LatLng> points;
    private final SparseArray<List<LatLng>> subLists;
    private final RouteAnimatorCallback callback;
    private volatile boolean requestedEnd = false;
    private ValueAnimator animator;

    RouteAnimator(@NonNull List<LatLng> points,
                  @NonNull RouteAnimatorCallback callback) {
        this.interpolatedPoints = new LinkedList<>();
        this.points = points;
        this.subLists = new SparseArray<>(101);
        this.callback = callback;
    }

    void start() {
        ObjectAnimator firstRunAnimator = ObjectAnimator.ofObject(this, "pointUpdate",
                new RouteEvaluator(), points.toArray());
        firstRunAnimator.setInterpolator(new LinearInterpolator());
        firstRunAnimator.setDuration(1600);
        firstRunAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!requestedEnd) {
                    animator.start();
                }
            }
        });

        animator = ValueAnimator.ofInt(0, 100);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(1600);
        animator.setStartDelay(300);

        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!requestedEnd) {
                    animation.setStartDelay(300);
                    animation.start();
                }
            }
        });

        animator.addUpdateListener(animation -> {

            int percentage = (int) animation.getAnimatedValue();
            if (subLists.get(percentage) != null) {
                callback.renderInterpolatedPoints(subLists.get(percentage));
                return;
            }
            int listSize = (int) (interpolatedPoints.size() * percentage / 100f);
            List<LatLng> points = interpolatedPoints.subList(0, Math.max(listSize, 3));
            subLists.put(percentage, points);
            callback.renderInterpolatedPoints(points);
        });

        firstRunAnimator.start();
    }

    void stop() {
        synchronized (this) {
            requestedEnd = true;
            animator.cancel();
        }
    }

    public void setPointUpdate(LatLng newPoint) {
        interpolatedPoints.add(newPoint);
        callback.renderInterpolatedPoints(interpolatedPoints);
    }

    interface RouteAnimatorCallback {
        void renderInterpolatedPoints(List<LatLng> points);
    }

    static class RouteEvaluator implements TypeEvaluator<LatLng> {

        @Override
        public LatLng evaluate(float t, LatLng startPoint, LatLng endPoint) {
            double lat = startPoint.latitude + t * (endPoint.latitude - startPoint.latitude);
            double lng = startPoint.longitude + t * (endPoint.longitude - startPoint.longitude);
            return new LatLng(lat, lng);
        }
    }
}
