package me.cassiano.estimate.widget.selection;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.Observable;
import me.cassiano.estimate.R;
import me.cassiano.estimate.data.presentation.model.JourneySelectionState;

import static android.support.v4.content.ContextCompat.getColor;
import static me.cassiano.estimate.utils.Drawables.tintCompoundDrawable;

public class JourneySelection extends ConstraintLayout implements JourneySelectionView {

    private Button pickOriginButton;
    private Button pickDestinationButton;

    public JourneySelection(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View.inflate(getContext(), R.layout.view_journey_selection, this);
        bindViews();
    }

    @Override
    public Observable<Boolean> pickOriginIntent() {
        return RxView
                .clicks(pickOriginButton)
                .map(it -> true);
    }

    @Override
    public Observable<Boolean> pickDestinationIntent() {
        return RxView
                .clicks(pickDestinationButton)
                .map(it -> true);
    }

    @Override
    public void render(JourneySelectionState state) {

        if (state.from() != null) {
            Place from = state.from();
            String message = TextUtils.isEmpty(from.getAddress())
                    ? latLngRepresentation(from.getLatLng())
                    : from.getAddress().toString();
            pickOriginButton.setText(message);
        } else pickOriginButton.setText(null);

        if (state.to() != null) {
            Place to = state.to();
            String message = TextUtils.isEmpty(to.getAddress())
                    ? latLngRepresentation(to.getLatLng())
                    : to.getAddress().toString();
            pickDestinationButton.setText(message);
        } else pickDestinationButton.setText(null);
    }

    private void bindViews() {
        pickOriginButton = findViewById(R.id.origin_button);
        pickDestinationButton = findViewById(R.id.destination_button);
        setBackgroundColor(Color.WHITE);
        setElevation(18);
        tintCompoundDrawable(pickOriginButton, getColor(getContext(), R.color.colorAccent), 0);
        tintCompoundDrawable(pickDestinationButton, getColor(getContext(), R.color.colorPrimary), 0);
    }

    private String latLngRepresentation(LatLng latLng) {
        return getContext().getString(R.string.format_lat_long,
                latLng.latitude, latLng.longitude);
    }

}
