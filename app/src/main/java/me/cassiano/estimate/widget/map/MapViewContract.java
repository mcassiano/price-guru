package me.cassiano.estimate.widget.map;

import io.reactivex.Observable;
import me.cassiano.estimate.data.presentation.model.MapViewState;

public interface MapViewContract {

    Observable<Boolean> clickedOnMyLocation();

    void render(MapViewState state);

}
