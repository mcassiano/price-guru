package me.cassiano.estimate.widget.selection;

import io.reactivex.Observable;
import me.cassiano.estimate.data.presentation.model.JourneySelectionState;

public interface JourneySelectionView {

    Observable<Boolean> pickOriginIntent();

    Observable<Boolean> pickDestinationIntent();

    void render(JourneySelectionState state);
}
