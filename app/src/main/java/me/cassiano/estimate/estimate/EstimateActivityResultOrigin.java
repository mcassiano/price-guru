package me.cassiano.estimate.estimate;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import io.reactivex.subjects.PublishSubject;
import me.cassiano.estimate.chain.ActivityResultChain;

import static android.app.Activity.RESULT_OK;

public class EstimateActivityResultOrigin extends ActivityResultChain {

    public static final int ORIGIN_REQUEST_CODE = 764;
    private final PublishSubject<Place> subject;

    public EstimateActivityResultOrigin(@NonNull PublishSubject<Place> subject) {
        this.subject = subject;
    }

    @Override
    protected boolean canHandle(int requestCode, int resultCode) {
        return resultCode == RESULT_OK &&
                requestCode == ORIGIN_REQUEST_CODE;
    }

    @Override
    protected void handle(@NonNull Context context, @NonNull Intent intent) {
        Place place = PlacePicker.getPlace(context, intent);
        if (place != null) subject.onNext(place);
    }
}
