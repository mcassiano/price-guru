package me.cassiano.estimate.estimate;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import me.cassiano.estimate.R;
import me.cassiano.estimate.data.model.local.Estimate;
import me.cassiano.estimate.data.model.local.ServiceEstimate;

import java.util.List;

import static java.util.Collections.emptyList;

public class EstimateListAdapter extends RecyclerView.Adapter<EstimateListAdapter.ViewHolder> {

    private ServiceEstimate estimates;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_estimate, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(estimates.getEstimates().get(position));
    }

    @Override
    public int getItemCount() {
        return estimates == null || estimates.getEstimates() == null ? 0
                : estimates.getEstimates().size();
    }

    public void setEstimates(ServiceEstimate estimates) {
        List<Estimate> newItems = estimates == null ?
                emptyList() : estimates.getEstimates();
        List<Estimate> old = this.estimates == null ?
                emptyList() : this.estimates.getEstimates();
        DiffUtil.Callback callback = new DiffCallback(old, newItems);
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(callback);
        this.estimates = estimates;
        result.dispatchUpdatesTo(this);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView priceAndEta;
        private TextView vehicleClass;
        private ImageView icon;

        private ViewHolder(View itemView) {
            super(itemView);
            priceAndEta = itemView.findViewById(R.id.eta_price);
            vehicleClass = itemView.findViewById(R.id.vehicle_class);
            icon = itemView.findViewById(R.id.icon);
        }

        void bind(Estimate estimate) {
            priceAndEta.setText(itemView.getContext().getString(R.string.format_price_eta,
                    estimate.getAverageEta(), estimate.getPriceFormatted()));
            vehicleClass.setText(estimate.getClassName());
            Picasso.with(itemView.getContext())
                    .load(estimate.getClassIconUrl())
                    .into(icon);
        }
    }

    private static class DiffCallback extends DiffUtil.Callback {

        private final List<Estimate> oldItems;
        private final List<Estimate> newItems;

        private DiffCallback(List<Estimate> oldItems, List<Estimate> newItems) {
            this.oldItems = oldItems == null ? emptyList() : oldItems;
            this.newItems = newItems == null ? emptyList() : newItems;
        }

        @Override
        public int getOldListSize() {
            return oldItems.size();
        }

        @Override
        public int getNewListSize() {
            return newItems.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition));
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return areItemsTheSame(oldItemPosition, newItemPosition);
        }
    }
}
