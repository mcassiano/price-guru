package me.cassiano.estimate.estimate;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.florent37.viewanimator.ViewAnimator;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import me.cassiano.estimate.DependencyInjection;
import me.cassiano.estimate.PriceGuruApp;
import me.cassiano.estimate.R;
import me.cassiano.estimate.chain.ActivityResultChain;
import me.cassiano.estimate.data.presentation.EstimateScreenViewModel;
import me.cassiano.estimate.data.presentation.EstimateScreenViewModel.NoRouteFound;
import me.cassiano.estimate.data.presentation.model.EstimateScreenViewState;
import me.cassiano.estimate.data.presentation.model.JourneySelectionState;
import me.cassiano.estimate.data.presentation.model.MapViewState;
import me.cassiano.estimate.data.presentation.model.PriceEstimatesViewState;
import me.cassiano.estimate.data.service.PlaceService;
import me.cassiano.estimate.maps.MapViewObserver;
import me.cassiano.estimate.utils.Toaster;
import me.cassiano.estimate.widget.estimates.PriceEstimates;
import me.cassiano.estimate.widget.map.GuruMapView;
import me.cassiano.estimate.widget.selection.JourneySelection;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.widget.Toast.LENGTH_LONG;
import static com.jakewharton.rxbinding2.view.RxView.layoutChangeEvents;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static me.cassiano.estimate.utils.AlertFactory.showAlertOK;

public class EstimateActivity extends AppCompatActivity {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 232;

    private final PublishSubject<Place> currentPlaceIntent = PublishSubject.create();
    private final PublishSubject<Place> originResult = PublishSubject.create();
    private final PublishSubject<Place> destinationResult = PublishSubject.create();
    private final PublishSubject<Boolean> backIntent = PublishSubject.create();
    private final PublishSubject<Boolean> locationPermissionGranted = PublishSubject.create();
    private final CompositeDisposable disposables = new CompositeDisposable();

    private GuruMapView mapView;
    private MapViewObserver mapViewObserver;
    private ConstraintLayout parentView;
    private JourneySelection journeySelectionView;
    private PriceEstimates priceEstimatesView;

    private Toaster toaster;
    private PickerAction originPickerAction;
    private PickerAction destinationPickerAction;
    private ActivityResultChain resultHandler;
    private PlaceService service;
    private GoogleApiClient apiClient;
    private ViewAnimator animator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimate);

        DependencyInjection di = ((PriceGuruApp) getApplication())
                .getDependencyInjection();

        parentView = findViewById(R.id.parentView);
        mapView = findViewById(R.id.map_view);
        journeySelectionView = findViewById(R.id.selection);
        priceEstimatesView = findViewById(R.id.estimates);

        mapViewObserver = new MapViewObserver(getLifecycle(), mapView);
        mapViewObserver.onCreate(savedInstanceState);
        resultHandler = getResultHandler();

        toaster = di.getToaster();
        originPickerAction = di.pickOriginTransformer(this);
        destinationPickerAction = di.pickDestinationTransformer(this);
        apiClient = di.getGoogleApiClient();
        service = di.getPlaceService();

        EstimateScreenViewModel viewModel = ViewModelProviders
                .of(this, di.getEstimateViewModelFactory())
                .get(EstimateScreenViewModel.class);

        viewModel.bindIntents(originIntent(), destinationIntent(),
                currentPlaceIntent, closeIntent());

        disposables.add(viewModel.observeState()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::render));

        disposables.add(RxView.layoutChangeEvents(journeySelectionView)
                .mergeWith(layoutChangeEvents(priceEstimatesView))
                .debounce(200, MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(change -> {
                    int height = change.view().getHeight();
                    if (height > 0)
                        mapView.setMapBottomOffset(height);
                }));

        disposables.add(mapView
                .clickedOnMyLocation()
                .subscribe(t -> getCurrentPlace(), t -> {
                }));


        if (savedInstanceState == null)
            getCurrentPlace();
        else if (!locationPermissionGranted())
            getCurrentPlace();
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (!resultHandler.handle(requestCode, resultCode, this, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            locationPermissionGranted.onNext(
                    grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED);
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {

        if (priceEstimatesView.getVisibility() == VISIBLE) {
            if (animator != null) {
                animator.cancel();
                animator = null;
            }
            backIntent.onNext(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        originPickerAction = null;
        destinationPickerAction = null;
        currentPlaceIntent.onComplete();
        originResult.onComplete();
        destinationResult.onComplete();
        backIntent.onComplete();
        locationPermissionGranted.onComplete();
        apiClient.disconnect();
        disposables.clear();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapViewObserver.onSaveInstance(outState);
    }

    private Observable<Place> destinationIntent() {
        return journeySelectionView
                .pickDestinationIntent()
                .flatMap(it -> checkLocationPermission())
                .filter(it -> it)
                .compose(destinationPickerAction.transform(destinationResult));
    }

    private Observable<Place> originIntent() {
        return journeySelectionView
                .pickOriginIntent()
                .flatMap(it -> checkLocationPermission())
                .filter(it -> it)
                .compose(originPickerAction.transform(originResult));
    }

    private Observable<Boolean> closeIntent() {
        return priceEstimatesView
                .closeEstimatesIntent()
                .mergeWith(backIntent);
    }

    private void render(EstimateScreenViewState state) {

        Log.d("Render process", state.toString());

        // finished loading estimates -> renders map, price estimate
        // started loading directions, estimates ->
        //          renders map,hide journey, show loading estimates
        // picked origin -> render map

        if (state.error() != null) {

            if (animator != null) {
                animator.cancel();
                animator = null;
            }

            state.error().printStackTrace();

            if (state.error() instanceof NoRouteFound) {
                toaster.makeText(this,
                        getString(R.string.message_no_route_error), LENGTH_LONG).show();
            } else {
                toaster.makeText(this,
                        getString(R.string.message_generic_error), LENGTH_LONG).show();
            }
        }

        if (state.estimates() != null) {
            // render map (show two points, polyline)
            // render price estimate
            journeySelectionView.setVisibility(GONE);
            priceEstimatesView.setVisibility(VISIBLE);

            priceEstimatesView.render(PriceEstimatesViewState
                    .create(false, state.estimates()));
            journeySelectionView.render(JourneySelectionState
                    .create(state.from(), state.to()));
            mapView.render(MapViewState
                    .create(state.from(), state.to())
                    .withDirections(state.directions()));

        } else if (state.loadingDirections()) {
            // render journey
            // animate/hide journey
            // animate/show loading (estimates)
            // render map (show two points, no polyline)

            journeySelectionView.render(JourneySelectionState
                    .create(state.from(), state.to()));

            animator = ViewAnimator
                    .animate(journeySelectionView)
                    .translationY(parentView.getHeight())
                    .andAnimate(priceEstimatesView)
                    .startDelay(100)
                    .translationY(0)
                    .onStart(() -> {
                        priceEstimatesView.setY(parentView.getHeight());
                        priceEstimatesView.setVisibility(VISIBLE);
                        priceEstimatesView.render(PriceEstimatesViewState
                                .create(true, null));
                    })
                    .onStop(() -> {
                        mapView.render(MapViewState
                                .create(state.from(), state.to())
                                .withLoadingDirections(true));
                        journeySelectionView.setVisibility(GONE);
                        animator = null;
                    })
                    .duration(400)
                    .start();

        } else if (priceEstimatesView.getVisibility() == VISIBLE) {
            // this is the going back state
            // animate journey in, estimates out
            // render journey
            // render map

            animator = ViewAnimator
                    .animate(journeySelectionView)
                    .translationY(0)
                    .startDelay(100)
                    .andAnimate(priceEstimatesView)
                    .translationY(parentView.getHeight())
                    .onStart(() -> {
                        journeySelectionView.setY(parentView.getHeight());
                        journeySelectionView.setVisibility(VISIBLE);
                        journeySelectionView.render(JourneySelectionState
                                .create(state.from(), state.to()));
                    })
                    .onStop(() -> {
                        mapView.render(MapViewState
                                .create(state.from(), state.to())
                                .withLoadingDirections(false));
                        priceEstimatesView.setVisibility(GONE);
                        animator = null;
                    })
                    .duration(400)
                    .start();

        } else {
            // render journey (origin point)
            // render map (origin point)
            journeySelectionView.render(JourneySelectionState
                    .create(state.from(), state.to()));
            mapView.render(MapViewState
                    .create()
                    .withFrom(state.from())
                    .withTo(state.to())
            );
        }

    }

    @SuppressLint("MissingPermission")
    private void getCurrentPlace() {

        disposables.add(checkLocationPermission()
                .filter(it -> it)
                .debounce(200, MILLISECONDS)
                .switchMap(it -> service.getCurrentPlace()
                        .toObservable()
                        .subscribeOn(Schedulers.io()))
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(currentPlaceIntent::onNext, Throwable::printStackTrace));
    }

    private ActivityResultChain getResultHandler() {
        return new ActivityResultChain.Builder()
                .next(new EstimateActivityResultOrigin(originResult))
                .next(new EstimateActivityResultDestination(destinationResult))
                .build();
    }

    private Observable<Boolean> checkLocationPermission() {

        if (!locationPermissionGranted()) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    ACCESS_FINE_LOCATION)) {
                showAlertOK(this,
                        R.string.message_location_permissions_needed,
                        (d, i) -> {
                            Intent intent = new Intent();
                            intent.setAction(ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            d.dismiss();
                        });
                return Observable.just(false);
            }

            requestLocationPermissions();
            return locationPermissionGranted;
        }

        return Observable.just(true);
    }

    private void requestLocationPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
    }

    private boolean locationPermissionGranted() {
        return ActivityCompat.checkSelfPermission(
                this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
    }

}
