package me.cassiano.estimate.estimate;


import com.google.android.gms.location.places.Place;

import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.PublishSubject;

public interface PickerAction {

    ObservableTransformer<Boolean, Place> transform(PublishSubject<Place> downstream);
}
