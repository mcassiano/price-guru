package me.cassiano.estimate.maps;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import me.cassiano.estimate.widget.map.GuruMapView;

public class MapViewObserver implements LifecycleObserver {

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    private final Lifecycle lifecycle;
    private GuruMapView mapView;

    public MapViewObserver(@NonNull Lifecycle lifecycle,
                           @NonNull GuruMapView mapView) {

        this.lifecycle = lifecycle;
        this.mapView = mapView;
        lifecycle.addObserver(this);
    }

    public void onCreate(Bundle bundle) {
        Bundle mapViewBundle = null;
        if (bundle != null) {
            mapViewBundle = bundle.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mapView.onCreate(mapViewBundle);
        mapView.onCreateCalled();
    }

    public void onSaveInstance(Bundle bundle) {
        Bundle mapViewBundle = bundle.getBundle(MAPVIEW_BUNDLE_KEY);

        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            bundle.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_RESUME)
    void onResume() {
        mapView.onResume();
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_START)
    void onStart() {
        mapView.onStart();
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_STOP)
    void onStop() {
        mapView.onStop();
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_PAUSE)
    void onPause() {
        mapView.onPause();
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    void onDestroy() {
        mapView.onDestroy();
        mapView = null;
        lifecycle.removeObserver(this);
    }
}
