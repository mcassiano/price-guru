package me.cassiano.estimate;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import java.io.IOException;

import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;

public class PriceGuruApp extends Application {

    private DependencyInjection dependencyInjection;

    @Override
    public void onCreate() {
        super.onCreate();
        dependencyInjection = new DependencyInjection(this);
        setUpRxJavaErrorHandler();
    }

    public DependencyInjection getDependencyInjection() {
        return dependencyInjection;
    }

    @VisibleForTesting
    public void setDependencyInjection(DependencyInjection dependencyInjection) {
        this.dependencyInjection = dependencyInjection;
    }

    private void setUpRxJavaErrorHandler() {
        RxJavaPlugins.setErrorHandler(e -> {
            if (e instanceof UndeliverableException) {
                e = e.getCause();
            }
            if (e instanceof IOException) {
                // fine, irrelevant network problem or API that throws on cancellation
                return;
            }
            if (e instanceof InterruptedException) {
                // fine, some blocking code was interrupted by a dispose call
                return;
            }
            if ((e instanceof NullPointerException) || (e instanceof IllegalArgumentException)) {
                // that's likely a bug in the application
                Thread.currentThread().getUncaughtExceptionHandler()
                        .uncaughtException(Thread.currentThread(), e);
                return;
            }
            if (e instanceof IllegalStateException) {
                // that's a bug in RxJava or in a custom operator
                Thread.currentThread().getUncaughtExceptionHandler()
                        .uncaughtException(Thread.currentThread(), e);
            }

        });
    }
}
