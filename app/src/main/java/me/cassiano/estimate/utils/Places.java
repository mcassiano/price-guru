package me.cassiano.estimate.utils;

import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;

public final class Places {

    private Places() {
    }

    public static Intent pickPlacesIntent(Activity activity) {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker
                    .IntentBuilder();
            return builder.build(activity);
        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
        return null;
    }

}
