package me.cassiano.estimate.utils;

import android.content.Context;
import android.widget.Toast;

public class Toaster {

    public Toast makeText(Context context, CharSequence text, int duration) {
        return Toast.makeText(context, text, duration);
    }

}
