package me.cassiano.estimate.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import me.cassiano.estimate.R;

public class AlertFactory {

    private AlertFactory() {
    }

    public static void showAlertOK(Context context, @StringRes int messageRes,
                                   DialogInterface.OnClickListener callback) {
        showAlertOK(context, context.getString(messageRes), callback);
    }

    public static void showAlertOK(Context context, String message,
                                   DialogInterface.OnClickListener callback) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(context.getText(R.string.action_ok), callback)
                .create()
                .show();
    }

}
