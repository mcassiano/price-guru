package me.cassiano.estimate.chain;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

public abstract class ActivityResultChain {

    private ActivityResultChain next;

    public boolean handle(int requestCode, int resultCode,
                          @NonNull Context context, @NonNull Intent intent) {

        if (canHandle(requestCode, resultCode)) {
            handle(context, intent);
            return true;
        } else if (next != null)
            return next.handle(requestCode, resultCode, context, intent);

        return false;
    }

    void setNext(@NonNull ActivityResultChain next) {
        if (this.next == null) this.next = next;
        else this.next.setNext(next);
    }

    protected abstract boolean canHandle(int requestCode, int resultCode);

    protected abstract void handle(@NonNull Context context, @NonNull Intent intent);

    public static final class Builder {

        private ActivityResultChain chainHead;

        public Builder next(ActivityResultChain next) {
            if (chainHead == null) chainHead = next;
            else chainHead.setNext(next);
            return this;
        }

        public ActivityResultChain build() {
            return chainHead;
        }

    }


}
