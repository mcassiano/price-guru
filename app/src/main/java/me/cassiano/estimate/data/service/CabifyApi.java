package me.cassiano.estimate.data.service;

import java.util.List;

import io.reactivex.Observable;
import me.cassiano.estimate.BuildConfig;
import me.cassiano.estimate.data.model.remote.cabify.EstimateRequest;
import me.cassiano.estimate.data.model.remote.cabify.EstimateResponse;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface CabifyApi {

    @Headers({
            "Authorization: Bearer " + BuildConfig.KEY_CABIFY
    })
    @POST("estimate")
    Observable<List<EstimateResponse>> getEstimates(@Body EstimateRequest request);

}
