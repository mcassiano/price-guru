package me.cassiano.estimate.data.presentation.model;

import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;
import me.cassiano.estimate.data.model.local.ServiceEstimate;

@AutoValue
public abstract class PriceEstimatesViewState {

    public abstract boolean loadingPrices();

    @Nullable
    public abstract ServiceEstimate estimates();

    public static PriceEstimatesViewState create(boolean loadingPrices,
                                                 ServiceEstimate estimates) {
        return new AutoValue_PriceEstimatesViewState(loadingPrices, estimates);
    }


}
