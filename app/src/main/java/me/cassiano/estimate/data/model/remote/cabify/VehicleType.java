package me.cassiano.estimate.data.model.remote.cabify;

import com.squareup.moshi.Json;

public class VehicleType {


    @Json(name = "_id")
    private String id;
    @Json(name = "name")
    private String name;
    @Json(name = "short_name")
    private String shortName;
    @Json(name = "description")
    private String description;
    @Json(name = "icons")
    private Icon icons;
    @Json(name = "icon")
    private String icon;
    @Json(name = "service_type")
    private String serviceType;
    @Json(name = "eta")
    private Eta eta;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getDescription() {
        return description;
    }

    public Icon getIcons() {
        return icons;
    }

    public String getIcon() {
        return icon;
    }

    public String getServiceType() {
        return serviceType;
    }

    public Eta getEta() {
        return eta;
    }

    public void setEta(Eta eta) {
        this.eta = eta;
    }

    public static final class Icon {

        @Json(name = "regular")
        private String regular;

        public String getRegular() {
            return regular;
        }

//        public void setRegular(String regular) {
//            this.regular = regular;
//        }
    }
}
