package me.cassiano.estimate.data.model.local;


import java.util.ArrayList;
import java.util.List;

public class ServiceEstimate {

    private String service;
    private boolean withinAreaOfOperation;
    private List<Estimate> estimates;

    public String getService() {
        return service;
    }

    public boolean isWithinAreaOfOperation() {
        return withinAreaOfOperation;
    }

    public List<Estimate> getEstimates() {
        return estimates;
    }

    public static ServiceEstimateBuilder builder() {
        return new ServiceEstimateBuilder();
    }


    public static final class ServiceEstimateBuilder {
        private String service;
        private boolean withinAreaOfOperation;
        private List<Estimate> estimates = new ArrayList<>();

        private ServiceEstimateBuilder() {
        }

        public ServiceEstimateBuilder service(String service) {
            this.service = service;
            return this;
        }

        public ServiceEstimateBuilder withinAreaOfOperation(boolean withinAreaOfOperation) {
            this.withinAreaOfOperation = withinAreaOfOperation;
            return this;
        }

        public ServiceEstimateBuilder addEstimate(Estimate estimate) {
            this.estimates.add(estimate);
            return this;
        }

        public ServiceEstimate build() {
            ServiceEstimate serviceEstimate = new ServiceEstimate();
            serviceEstimate.withinAreaOfOperation = this.withinAreaOfOperation;
            serviceEstimate.estimates = this.estimates;
            serviceEstimate.service = this.service;
            return serviceEstimate;
        }
    }
}
