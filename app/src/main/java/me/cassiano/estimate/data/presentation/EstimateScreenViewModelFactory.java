package me.cassiano.estimate.data.presentation;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import me.cassiano.estimate.data.EstimateRepository;
import me.cassiano.estimate.data.service.PlaceService;

public class EstimateScreenViewModelFactory implements ViewModelProvider.Factory {

    private final PlaceService placeService;
    private final EstimateRepository estimateRepository;

    public EstimateScreenViewModelFactory(PlaceService placeService,
                                          EstimateRepository estimateRepository) {
        this.placeService = placeService;
        this.estimateRepository = estimateRepository;
    }

    @SuppressWarnings({"unchecked"})
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(EstimateScreenViewModel.class))
            return (T) new EstimateScreenViewModel(placeService,
                    estimateRepository);

        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
