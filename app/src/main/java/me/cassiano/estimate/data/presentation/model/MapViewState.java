package me.cassiano.estimate.data.presentation.model;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.google.auto.value.AutoValue;

import java.util.List;

import io.reactivex.annotations.Nullable;

@AutoValue
public abstract class MapViewState {

    @Nullable
    public abstract Place from();

    @Nullable
    public abstract Place to();

    public abstract boolean loadingDirections();

    @Nullable
    public abstract List<LatLng> directions();

    public abstract MapViewState withFrom(Place from);

    public abstract MapViewState withTo(Place to);

    public abstract MapViewState withLoadingDirections(boolean loadingDirections);

    public abstract MapViewState withDirections(List<LatLng> directions);

    public static MapViewState create(Place from, Place to) {
        return new AutoValue_MapViewState(from, to,
                false, null);
    }

    public static MapViewState create() {
        return new AutoValue_MapViewState(null, null,
                false, null);
    }


}
