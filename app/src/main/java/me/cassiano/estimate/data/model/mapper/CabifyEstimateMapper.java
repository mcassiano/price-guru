package me.cassiano.estimate.data.model.mapper;


import java.util.List;

import me.cassiano.estimate.data.model.local.Estimate;
import me.cassiano.estimate.data.model.local.ServiceEstimate;
import me.cassiano.estimate.data.model.local.ServiceEstimate.ServiceEstimateBuilder;
import me.cassiano.estimate.data.model.remote.cabify.EstimateResponse;
import me.cassiano.estimate.data.model.remote.cabify.Eta;

public class CabifyEstimateMapper {

    private static final String CABIFY = "Cabify";

    private CabifyEstimateMapper() {
    }

    public static ServiceEstimate map(List<EstimateResponse> response) {

        if (response == null || response.isEmpty()) {
            return ServiceEstimate
                    .builder()
                    .service(CABIFY)
                    .withinAreaOfOperation(false)
                    .build();
        }

        ServiceEstimateBuilder builder = ServiceEstimate.builder()
                .service(CABIFY)
                .withinAreaOfOperation(true);

        for (EstimateResponse estimate : response) {
            builder.addEstimate(Estimate
                    .builder()
                    .totalPrice(estimate.getTotalPrice())
                    .priceFormatted(estimate.getPriceFormatted())
                    .averageEta(mapEta(estimate.getVehicleType().getEta()))
                    .classDescription(estimate.getVehicleType().getDescription())
                    .className(estimate.getVehicleType().getShortName())
                    .classIconUrl(estimate.getVehicleType().getIcons().getRegular())
                    .classId(estimate.getVehicleType().getId())
                    .currency(estimate.getCurrency())
                    .currencySymbol(estimate.getCurrencySymbol())
                    .build());
        }

        return builder.build();
    }

    private static String mapEta(Eta eta) {
        if (eta == null) return "-";
        return eta.getFormatted();
    }
}
