package me.cassiano.estimate.data.presentation.model;

import com.google.android.gms.location.places.Place;
import com.google.auto.value.AutoValue;

import io.reactivex.annotations.Nullable;

@AutoValue
public abstract class JourneySelectionState {

    @Nullable
    public abstract Place from();

    @Nullable
    public abstract Place to();

    public abstract JourneySelectionState withFrom(Place from);

    public abstract JourneySelectionState withTo(Place to);

    public static JourneySelectionState create() {
        return create(null, null);
    }

    public static JourneySelectionState create(@Nullable Place from,
                                               @Nullable Place to) {
        return new AutoValue_JourneySelectionState(from, to);
    }
}
