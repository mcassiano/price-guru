package me.cassiano.estimate.data;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Maybe;
import me.cassiano.estimate.BuildConfig;
import me.cassiano.estimate.data.service.PlaceService;

import static com.google.android.gms.location.places.Places.PlaceDetectionApi;
import static com.google.maps.android.PolyUtil.decode;
import static com.google.maps.android.PolyUtil.simplify;
import static com.google.maps.model.TravelMode.DRIVING;
import static io.reactivex.Maybe.defer;
import static java.util.Collections.emptyList;

public class AndroidPlaceService implements PlaceService {

    private GeoApiContext apiContext;
    private GoogleApiClient client;

    public AndroidPlaceService(@NonNull GoogleApiClient client) {
        this.client = client;
        this.apiContext = new GeoApiContext();
        this.apiContext
                .setQueryRateLimit(3)
                .setApiKey(BuildConfig.KEY_DIRECTIONS)
                .setConnectTimeout(60, TimeUnit.SECONDS)
                .setReadTimeout(60, TimeUnit.SECONDS)
                .setWriteTimeout(60, TimeUnit.SECONDS);
    }

    @Override
    public Maybe<Place> getCurrentPlace() {
        return defer(() -> {
            Place place;

            try {
                place = currentPlace();
            } catch (Exception e) {
                return Maybe.error(e);
            }

            return place == null ? Maybe.empty() : Maybe.just(place);
        });
    }

    @Override
    public Maybe<List<LatLng>> getDrivingDirections(LatLng from, final LatLng to) {
        return Maybe.fromCallable(() -> drivingDirections(from, to));
    }


    @SuppressLint("MissingPermission")
    private Place currentPlace() {
        assureGoogleApiConnected();
        PlaceLikelihoodBuffer buffer = PlaceDetectionApi
                .getCurrentPlace(client, null)
                .await();

        Place place = buffer.getCount() > 0 ?
                buffer.get(0).getPlace().freeze() : null;
        buffer.release();
        return place;
    }

    private List<LatLng> drivingDirections(LatLng from, LatLng to)
            throws InterruptedException, ApiException, IOException {

        DirectionsResult result = DirectionsApi.newRequest(apiContext)
                .mode(DRIVING)
                .origin(new com.google.maps.model.LatLng(from.latitude, from.longitude))
                .destination(new com.google.maps.model.LatLng(to.latitude, to.longitude))
                .departureTime(new DateTime())
                .await();

        List<LatLng> path = emptyList();

        if (result.routes.length > 0) {
            DirectionsRoute route = result.routes[0];
            path = decode(route.overviewPolyline.getEncodedPath());
            path = simplify(path, 7);
        }

        return path;
    }

    private void assureGoogleApiConnected() {
        if (!client.isConnected()) {
            ConnectionResult result = client.blockingConnect();
            if (!result.isSuccess()) throw new IllegalStateException("Could not connect to API");
        }
    }
}
