package me.cassiano.estimate.data.model.remote.cabify;


import com.squareup.moshi.Json;

import java.util.List;

public class Stop {

    @Json(name = "loc")
    private List<Double> loc = null;
    @Json(name = "name")
    private String name;
    @Json(name = "addr")
    private String addr;

    public List<Double> getLocation() {
        return loc;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return addr;
    }

    public void setLocation(List<Double> loc) {
        this.loc = loc;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String addr) {
        this.addr = addr;
    }

    static StopBuilder builder() {
        return new StopBuilder();
    }

    public static final class StopBuilder {
        private List<Double> loc = null;
        private String name;
        private String addr;

        private StopBuilder() {
        }

        StopBuilder loc(List<Double> loc) {
            this.loc = loc;
            return this;
        }

        StopBuilder name(String name) {
            this.name = name;
            return this;
        }

        StopBuilder addr(String addr) {
            this.addr = addr;
            return this;
        }

        public Stop build() {
            Stop stop = new Stop();
            stop.addr = this.addr;
            stop.loc = this.loc;
            stop.name = this.name;
            return stop;
        }
    }
}
