package me.cassiano.estimate.data.model.remote.cabify;

import com.google.android.gms.location.places.Place;
import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EstimateRequest {

    @Json(name = "stops")
    private List<Stop> stops = null;
    @Json(name = "start_at")
    private String startAt;

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public String getStartAt() {
        return startAt;
    }


    public static EstimateRequest from(Place from, Place to) {
        return builder()
                .withStop(Stop
                        .builder()
                        .loc(Arrays.asList(from.getLatLng().latitude, from.getLatLng().longitude))
                        .addr(from.getAddress().toString())
                        .name(from.getName().toString())
                        .build())
                .withStop(Stop
                        .builder()
                        .loc(Arrays.asList(to.getLatLng().latitude, to.getLatLng().longitude))
                        .addr(to.getAddress().toString())
                        .name(to.getName().toString())
                        .build())
                .build();
    }

    private static EstimateRequestBuilder builder() {
        return new EstimateRequestBuilder();
    }

    static final class EstimateRequestBuilder {
        private List<Stop> stops = new ArrayList<>();
        private String startAt = null;

        private EstimateRequestBuilder() {
        }

        EstimateRequestBuilder withStop(Stop stop) {
            this.stops.add(stop);
            return this;
        }

        EstimateRequestBuilder startAt(String startAt) {
            this.startAt = startAt;
            return this;
        }

        EstimateRequest build() {
            EstimateRequest estimateRequest = new EstimateRequest();
            estimateRequest.startAt = this.startAt;
            estimateRequest.stops = this.stops;
            return estimateRequest;
        }
    }
}
