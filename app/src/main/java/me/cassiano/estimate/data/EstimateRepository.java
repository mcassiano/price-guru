package me.cassiano.estimate.data;

import com.google.android.gms.location.places.Place;

import io.reactivex.Observable;
import io.reactivex.exceptions.Exceptions;
import me.cassiano.estimate.data.model.local.ServiceEstimate;
import me.cassiano.estimate.data.model.mapper.CabifyEstimateMapper;
import me.cassiano.estimate.data.model.remote.cabify.EstimateRequest;
import me.cassiano.estimate.data.service.CabifyApi;
import retrofit2.HttpException;

public class EstimateRepository {

    private final CabifyApi cabifyApi;

    public EstimateRepository(CabifyApi cabifyApi) {
        this.cabifyApi = cabifyApi;
    }

    public Observable<ServiceEstimate> getEstimates(Place from, Place to) {
        return cabifyApi
                .getEstimates(EstimateRequest.from(from, to))
                .map(CabifyEstimateMapper::map)
                .onErrorReturn(error -> {
                    if (error instanceof HttpException) {
                        HttpException httpError = (HttpException) error;
                        if (httpError.code() == 404)
                            return CabifyEstimateMapper.map(null);
                    }
                    throw Exceptions.propagate(error);
                });
    }

}
