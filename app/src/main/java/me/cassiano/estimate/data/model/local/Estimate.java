package me.cassiano.estimate.data.model.local;

public class Estimate {

    private String classId;
    private String className;
    private String classDescription;
    private String classIconUrl;

    private Integer totalPrice;
    private String priceFormatted;
    private String currency;
    private String currencySymbol;
    private String averageEta;

    public String getClassId() {
        return classId;
    }

    public String getClassName() {
        return className;
    }

    public String getClassDescription() {
        return classDescription;
    }

    public String getClassIconUrl() {
        return classIconUrl;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public String getPriceFormatted() {
        return priceFormatted;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getAverageEta() {
        return averageEta;
    }

    public boolean isPriceAvailable() {
        return totalPrice != null;
    }

    public static EstimateBuilder builder() {
        return new EstimateBuilder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Estimate estimate = (Estimate) o;

        if (classId != null ? !classId.equals(estimate.classId) : estimate.classId != null)
            return false;
        if (className != null ? !className.equals(estimate.className) : estimate.className != null)
            return false;
        if (classDescription != null ? !classDescription.equals(estimate.classDescription) : estimate.classDescription != null)
            return false;
        if (classIconUrl != null ? !classIconUrl.equals(estimate.classIconUrl) : estimate.classIconUrl != null)
            return false;
        if (totalPrice != null ? !totalPrice.equals(estimate.totalPrice) : estimate.totalPrice != null)
            return false;
        if (priceFormatted != null ? !priceFormatted.equals(estimate.priceFormatted) : estimate.priceFormatted != null)
            return false;
        if (currency != null ? !currency.equals(estimate.currency) : estimate.currency != null)
            return false;
        if (currencySymbol != null ? !currencySymbol.equals(estimate.currencySymbol) : estimate.currencySymbol != null)
            return false;
        return averageEta != null ? averageEta.equals(estimate.averageEta) : estimate.averageEta == null;
    }

    @Override
    public int hashCode() {
        int result = classId != null ? classId.hashCode() : 0;
        result = 31 * result + (className != null ? className.hashCode() : 0);
        result = 31 * result + (classDescription != null ? classDescription.hashCode() : 0);
        result = 31 * result + (classIconUrl != null ? classIconUrl.hashCode() : 0);
        result = 31 * result + (totalPrice != null ? totalPrice.hashCode() : 0);
        result = 31 * result + (priceFormatted != null ? priceFormatted.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (currencySymbol != null ? currencySymbol.hashCode() : 0);
        result = 31 * result + (averageEta != null ? averageEta.hashCode() : 0);
        return result;
    }

    public static final class EstimateBuilder {
        private String classId;
        private String className;
        private String classDescription;
        private String classIconUrl;
        private Integer totalPrice;
        private String priceFormatted;
        private String currency;
        private String currencySymbol;
        private String averageEta;

        private EstimateBuilder() {
        }

        public EstimateBuilder classId(String classId) {
            this.classId = classId;
            return this;
        }

        public EstimateBuilder className(String className) {
            this.className = className;
            return this;
        }

        public EstimateBuilder classDescription(String classDescription) {
            this.classDescription = classDescription;
            return this;
        }

        public EstimateBuilder classIconUrl(String classIconUrl) {
            this.classIconUrl = classIconUrl;
            return this;
        }

        public EstimateBuilder totalPrice(Integer totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public EstimateBuilder priceFormatted(String priceFormatted) {
            this.priceFormatted = priceFormatted;
            return this;
        }

        public EstimateBuilder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public EstimateBuilder currencySymbol(String currencySymbol) {
            this.currencySymbol = currencySymbol;
            return this;
        }

        public EstimateBuilder averageEta(String averageEta) {
            this.averageEta = averageEta;
            return this;
        }

        public Estimate build() {
            Estimate estimate = new Estimate();
            estimate.classIconUrl = this.classIconUrl;
            estimate.totalPrice = this.totalPrice;
            estimate.averageEta = this.averageEta;
            estimate.currency = this.currency;
            estimate.className = this.className;
            estimate.classId = this.classId;
            estimate.currencySymbol = this.currencySymbol;
            estimate.classDescription = this.classDescription;
            estimate.priceFormatted = this.priceFormatted;
            return estimate;
        }
    }
}
