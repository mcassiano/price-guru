package me.cassiano.estimate.data.presentation;


import android.arch.lifecycle.ViewModel;

import com.google.android.gms.location.places.Place;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.BehaviorSubject;
import me.cassiano.estimate.data.EstimateRepository;
import me.cassiano.estimate.data.presentation.model.EstimateScreenViewState;
import me.cassiano.estimate.data.service.PlaceService;

import static io.reactivex.schedulers.Schedulers.newThread;
import static me.cassiano.estimate.data.presentation.model.EstimateScreenViewState.create;

public class EstimateScreenViewModel extends ViewModel {

    private final BehaviorSubject<EstimateScreenViewState>
            state = BehaviorSubject.create();
    private final PlaceService placeService;
    private final EstimateRepository repository;

    public EstimateScreenViewModel(PlaceService placeService,
                                   EstimateRepository repository) {
        this.placeService = placeService;
        this.repository = repository;
    }

    @Override
    protected void onCleared() {
        state.onComplete();
        super.onCleared();
    }

    public Observable<EstimateScreenViewState> observeState() {
        return state;
    }

    public EstimateScreenViewState currentRenderedState() {
        return state.getValue();
    }

    public void bindIntents(Observable<Place> origin,
                            Observable<Place> destination,
                            Observable<Place> currentLocation,
                            Observable<Boolean> closeEstimates) {

        EstimateScreenViewState seed =
                currentRenderedState() == null ? create() : state.getValue();

        Observable<EstimateScreenViewState> partial = Observable
                .merge(origin.map(it -> create()
                                .withFrom(it))
                                .compose(errorHandler),
                        currentLocation
                                .map(it -> create().withFrom(it))
                                .compose(errorHandler),
                        destination.map(it -> create()
                                .withTo(it))
                                .compose(errorHandler),
                        closeEstimates.map(it -> create())
                                .compose(errorHandler));

        partial
                .scan(seed, (previousPartial, currentPartial) -> { // partial states
                    // previous might have both from and to
                    // current will only have either to or from
                    if (currentRenderedState().error() != null && currentPartial.from() != null)
                        return create().withFrom(currentPartial.from()).withTo(null);
                    if (currentPartial.from() == null)
                        return create().withFrom(previousPartial.from()).withTo(currentPartial.to());
                    return create().withFrom(currentPartial.from()).withTo(previousPartial.to());
                })
                .switchMap(it -> { // unsubscribes from an ongoing request if close is triggered

                    if (it == seed) return Observable.just(it);

                    if (it.from() != null && it.to() != null) {
                        return placeService
                                .getDrivingDirections(it.from().getLatLng(), it.to().getLatLng())
                                .map(it::withDirections)
                                .toObservable()
                                .switchMap(state -> {
                                    if (state.directions().isEmpty())
                                        return Observable.just(create()
                                                .withFrom(state.from())
                                                .withError(new NoRouteFound()));
                                    return repository
                                            .getEstimates(state.from(), state.to())
                                            .map(state::withEstimates);
                                })
                                .startWith(it.withLoading(true))
                                .compose(errorHandler)
                                .subscribeOn(newThread());
                    }

                    return Observable.just(it.withDirections(null));
                })
                .compose(errorHandler)
                .subscribe(state);
    }

    private ObservableTransformer<EstimateScreenViewState, EstimateScreenViewState> errorHandler =
            upstream -> upstream.onErrorReturn(it -> create()
                    .withFrom(currentRenderedState().from())
                    .withError(it));


    public static class NoRouteFound extends Exception {

        @Override
        public boolean equals(Object obj) {
            return obj instanceof NoRouteFound;
        }
    }

}
