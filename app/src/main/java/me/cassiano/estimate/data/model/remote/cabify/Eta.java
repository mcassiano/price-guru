package me.cassiano.estimate.data.model.remote.cabify;

import com.squareup.moshi.Json;


public final class Eta {

    @Json(name = "min")
    private int min;
    @Json(name = "max")
    private int max;
    @Json(name = "formatted")
    private String formatted;

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public String getFormatted() {
        return formatted;
    }
}
