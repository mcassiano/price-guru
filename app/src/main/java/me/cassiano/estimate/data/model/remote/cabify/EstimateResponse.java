package me.cassiano.estimate.data.model.remote.cabify;


import com.squareup.moshi.Json;

public class EstimateResponse {

    @Json(name = "vehicle_type")
    private VehicleType vehicleType;
    @Json(name = "total_price")
    private Integer totalPrice;
    @Json(name = "price_formatted")
    private String priceFormatted;
    @Json(name = "currency")
    private String currency;
    @Json(name = "currency_symbol")
    private String currencySymbol;

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public String getPriceFormatted() {
        return priceFormatted;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setPriceFormatted(String priceFormatted) {
        this.priceFormatted = priceFormatted;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }


}
