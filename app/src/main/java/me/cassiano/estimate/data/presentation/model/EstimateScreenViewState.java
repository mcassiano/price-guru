package me.cassiano.estimate.data.presentation.model;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.google.auto.value.AutoValue;

import java.util.List;

import io.reactivex.annotations.Nullable;
import me.cassiano.estimate.data.model.local.ServiceEstimate;

@AutoValue
public abstract class EstimateScreenViewState {

    @Nullable
    public abstract Place from();

    @Nullable
    public abstract Place to();

    @Nullable
    public abstract List<LatLng> directions();

    public abstract boolean loadingDirections();

    @Nullable
    public abstract ServiceEstimate estimates();

    @Nullable
    public abstract Throwable error();

    public static EstimateScreenViewState create() {
        return create(null, null,
                null, false, null, null);
    }

    public static EstimateScreenViewState create(
            @Nullable Place from,
            @Nullable Place to,
            @Nullable List<LatLng> directions,
            boolean loadingDirections,
            @Nullable ServiceEstimate estimates,
            @Nullable Throwable error) {
        return new AutoValue_EstimateScreenViewState(from,
                to, directions, loadingDirections, estimates, error);
    }

    public abstract EstimateScreenViewState withFrom(Place from);

    public abstract EstimateScreenViewState withTo(Place to);

    public abstract EstimateScreenViewState withLoading(boolean loadingDirections);

    public abstract EstimateScreenViewState withDirections(List<LatLng> directions);

    public abstract EstimateScreenViewState withEstimates(ServiceEstimate estimates);

    public abstract EstimateScreenViewState withError(Throwable error);


}
