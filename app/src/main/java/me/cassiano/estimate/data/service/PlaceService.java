package me.cassiano.estimate.data.service;


import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.reactivex.Maybe;

public interface PlaceService {

    Maybe<Place> getCurrentPlace();

    Maybe<List<LatLng>> getDrivingDirections(LatLng from, LatLng to);

}
