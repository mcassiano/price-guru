package me.cassiano.estimate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;

import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import me.cassiano.estimate.data.AndroidPlaceService;
import me.cassiano.estimate.data.EstimateRepository;
import me.cassiano.estimate.data.presentation.EstimateScreenViewModelFactory;
import me.cassiano.estimate.data.service.CabifyApi;
import me.cassiano.estimate.data.service.PlaceService;
import me.cassiano.estimate.estimate.PickerAction;
import me.cassiano.estimate.utils.Toaster;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static com.google.android.gms.location.places.Places.GEO_DATA_API;
import static com.google.android.gms.location.places.Places.PLACE_DETECTION_API;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static me.cassiano.estimate.estimate.EstimateActivityResultDestination.DESTINATION_REQUEST_CODE;
import static me.cassiano.estimate.estimate.EstimateActivityResultOrigin.ORIGIN_REQUEST_CODE;
import static me.cassiano.estimate.utils.Places.pickPlacesIntent;
import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

public class DependencyInjection {

    public static final String BASE_URL = "https://test.cabify.com/api/v2/";
    private Context context;
    private GoogleApiClient client;
    private PlaceService placeService;
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private CabifyApi cabifyApi;
    private EstimateRepository estimateRepository;
    private EstimateScreenViewModelFactory estimateViewModelFactory;
    private Toaster toaster = new Toaster();

    DependencyInjection(Context context) {
        this.context = context;
    }

    public String getBaseUrl() {
        return BASE_URL;
    }

    public GoogleApiClient getGoogleApiClient() {

        if (client == null || !client.isConnected()) {
            client = new GoogleApiClient
                    .Builder(context)
                    .addApi(GEO_DATA_API)
                    .addApi(PLACE_DETECTION_API)
                    .build();
        }

        return client;
    }

    public PlaceService getPlaceService() {
        if (placeService == null) {
            placeService = new AndroidPlaceService(getGoogleApiClient());
        }
        return placeService;
    }

    public OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(BODY);

            Interceptor languageInterceptor = chain -> {
                Request.Builder builder = chain.request().newBuilder();

                builder.addHeader("Accept-Language", Locale.getDefault().toLanguageTag());
                Request request = builder.build();

                return chain.proceed(request);
            };

            okHttpClient = new OkHttpClient.Builder()
                    .addNetworkInterceptor(languageInterceptor)
                    .addInterceptor(loggingInterceptor)
                    .build();
        }

        return okHttpClient;
    }

    public Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(getOkHttpClient())
                    .baseUrl(getBaseUrl())
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public CabifyApi getCabifyApi() {
        if (cabifyApi == null) {
            cabifyApi = getRetrofit()
                    .create(CabifyApi.class);
        }
        return cabifyApi;
    }

    public EstimateRepository getEstimateRepository() {
        if (estimateRepository == null) {
            estimateRepository = new EstimateRepository(getCabifyApi());
        }

        return estimateRepository;
    }

    public EstimateScreenViewModelFactory getEstimateViewModelFactory() {
        if (estimateViewModelFactory == null) {
            estimateViewModelFactory = new EstimateScreenViewModelFactory(
                    getPlaceService(), getEstimateRepository());
        }

        return estimateViewModelFactory;
    }

    public PickerAction pickOriginTransformer(AppCompatActivity activity) {
        return new GooglePlacePicker(activity, ORIGIN_REQUEST_CODE);
    }

    public PickerAction pickDestinationTransformer(AppCompatActivity activity) {
        return new GooglePlacePicker(activity, DESTINATION_REQUEST_CODE);
    }

    public Toaster getToaster() {
        return toaster;
    }

    static class GooglePlacePicker implements PickerAction {

        private int requestCode;
        private Activity activity;

        GooglePlacePicker(Activity activity, int requestCode) {
            this.requestCode = requestCode;
            this.activity = activity;
        }

        @Override
        public ObservableTransformer<Boolean, Place> transform(PublishSubject<Place> downstream) {
            return new Transformer(activity, downstream, requestCode);
        }

        static class Transformer implements ObservableTransformer<Boolean, Place> {

            private Activity activity;
            private Subject<Place> downstream;
            private int requestCode;

            Transformer(Activity activity, Subject<Place> downstream, int requestCode) {
                this.activity = activity;
                this.downstream = downstream;
                this.requestCode = requestCode;
            }

            @Override
            public ObservableSource<Place> apply(Observable<Boolean> upstream) {
                return upstream
                        .doOnNext(it -> {
                            Intent intent = pickPlacesIntent(activity);
                            if (intent != null)
                                activity.startActivityForResult(intent, requestCode);
                        })
                        .switchMap(it -> downstream)
                        .delay(200, MILLISECONDS);
            }
        }
    }
}
