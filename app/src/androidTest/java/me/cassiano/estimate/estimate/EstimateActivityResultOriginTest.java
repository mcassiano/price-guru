package me.cassiano.estimate.estimate;

import android.content.Intent;
import android.os.Parcel;

import com.google.android.gms.location.places.Place;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import me.cassiano.estimate.utils.SafeParcelWriter;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static me.cassiano.estimate.estimate.EstimateActivityResultDestination.DESTINATION_REQUEST_CODE;
import static me.cassiano.estimate.estimate.EstimateActivityResultOrigin.ORIGIN_REQUEST_CODE;


@SuppressWarnings("unchecked")
public class EstimateActivityResultOriginTest {

    private EstimateActivityResultOrigin originChainNode;
    private PublishSubject<Place> testSubject;

    @Before
    public void setUp() throws Exception {
        testSubject = PublishSubject.create();
        originChainNode = new EstimateActivityResultOrigin(testSubject);
    }

    @Test
    public void canHandle_shouldReturnTrueForMatchingRequestAndResultCode() throws Exception {
        boolean canHandle = originChainNode.canHandle(ORIGIN_REQUEST_CODE, RESULT_OK);
        assertTrue(canHandle);
    }

    @Test
    public void canHandle_shouldReturnFalseForResultCodeCanceled() throws Exception {
        boolean canHandle = originChainNode.canHandle(ORIGIN_REQUEST_CODE, RESULT_CANCELED);
        assertFalse(canHandle);
    }

    @Test
    public void canHandle_shouldReturnFalseForOtherRequestCodes() throws Exception {
        boolean canHandle = originChainNode.canHandle(DESTINATION_REQUEST_CODE, RESULT_OK);
        assertFalse(canHandle);
    }

    @Test
    public void handle_shouldEmmitCorrectValuesWhenHandleIsCalled() throws Exception {
        ReplaySubject<Place> replay = ReplaySubject.create();
        testSubject.subscribe(replay);

        originChainNode.handle(getTargetContext(),
                getPickerResultIntent("Madrid"));

        originChainNode.handle(getTargetContext(),
                getPickerResultIntent("Paris"));

        Observable<Place> results = replay.take(2);

        Place madrid = results.elementAt(0).blockingGet();
        Place paris = results.elementAt(1).blockingGet();

        assertEquals("Madrid", madrid.getName());
        assertEquals("Paris", paris.getName());
    }

    @Test
    public void handle_shouldNotEmmitValuesWhenIntentIsEmpty() throws Exception {
        ReplaySubject<Place> replay = ReplaySubject.create();

        testSubject.subscribe(replay);
        originChainNode.handle(getTargetContext(), new Intent());
        testSubject.onComplete();

        long count = replay.count().blockingGet();
        assertEquals(0, count);
    }

    private Intent getPickerResultIntent(String name) {
        Intent intent = new Intent();
//        // the hackiest thing I've ever done (might break with updates to the places library!)
//        PlaceEntity mockPlaceEntity = new PlaceEntity.zza()
//                .zzii(name)
//                .zzae(Collections.emptyList())
//                .zzaf(Collections.emptyList())
//                .zzavt();
////        mockPlaceEntity.writeToParcel(parcel, 0);


        intent.putExtra("selected_place", marshallPlaceWithName(name));
        return intent;
    }

    private byte[] marshallPlaceWithName(String name) {
        Parcel parcel = Parcel.obtain();

        int start = SafeParcelWriter.writeStart(parcel);
        SafeParcelWriter.writeStringList(parcel, 17, Collections.emptyList(), false);
        SafeParcelWriter.write(parcel, 19, name, false);
        SafeParcelWriter.write(parcel, 20, Collections.emptyList(), false);
        SafeParcelWriter.writeEnd(parcel, start);

        byte[] binary = parcel.marshall();
        parcel.recycle();
        return binary;
    }

}