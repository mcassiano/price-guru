package me.cassiano.estimate.estimate;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.PublishSubject;
import io.victoralbertos.device_animation_test_rule.DeviceAnimationTestRule;
import me.cassiano.estimate.data.presentation.EstimateScreenViewModel.NoRouteFound;
import me.cassiano.estimate.data.service.PlaceService;
import me.cassiano.estimate.PriceGuruApp;
import me.cassiano.estimate.R;
import me.cassiano.estimate.TestDependencyInjection;
import me.cassiano.estimate.utils.IdlingToaster;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.Visibility.GONE;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static me.cassiano.estimate.utils.ToastMatcher.ofToast;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
public class EstimateActivityTest {

    private static final String FROM = "R. Quintana, 549";
    private static final LatLng FROM_LAT_LON = new LatLng(-23.6043058, -46.6921347);
    private static final String TO = "R. Lisboa, 890";
    private static final LatLng TO_LAT_LON = new LatLng(-23.5569912, -46.6833528);

    @ClassRule
    static public DeviceAnimationTestRule
            deviceAnimationTestRule = new DeviceAnimationTestRule();

    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule
            .grant(android.Manifest.permission.ACCESS_FINE_LOCATION);

    @Rule
    public ActivityTestRule<EstimateActivity> activityTestRule =
            new ActivityTestRule<>(EstimateActivity.class, false, false);

    private PublishSubject<Place> originTestSubject;
    private PublishSubject<Place> destinationTestSubject;
    private PlaceService service;
    private IdlingToaster toaster;

    @Before
    public void setUp() {

        RxJavaPlugins.setComputationSchedulerHandler(schedulerCallable ->
                io.reactivex.schedulers.Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR));

        RxJavaPlugins.setInitIoSchedulerHandler(schedulerCallable ->
                io.reactivex.schedulers.Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR));

        RxJavaPlugins.setNewThreadSchedulerHandler(scheduler ->
                io.reactivex.schedulers.Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR));

        PriceGuruApp app = (PriceGuruApp) InstrumentationRegistry
                .getTargetContext().getApplicationContext();

        TestDependencyInjection dependencyInjection = new TestDependencyInjection(app);
        app.setDependencyInjection(dependencyInjection);

        toaster = (IdlingToaster) dependencyInjection.getToaster();
        originTestSubject = PublishSubject.create();
        destinationTestSubject = PublishSubject.create();
        service = dependencyInjection.getPlaceService();

        PickerAction originPickerAction = dependencyInjection
                .pickOriginTransformer(activityTestRule.getActivity());

        PickerAction destinationPickerAction = dependencyInjection
                .pickDestinationTransformer(activityTestRule.getActivity());

        when(originPickerAction.transform(any())).thenReturn(
                upstream -> upstream.switchMap(it -> originTestSubject));

        when(destinationPickerAction.transform(any())).thenReturn(
                upstream -> upstream.switchMap(it -> destinationTestSubject));

    }

    @Test
    public void render_shouldShowCurrentPlace() throws InterruptedException {

        Place place = setUpFromMock();
        when(service.getCurrentPlace()).thenReturn(Maybe.just(place));

        activityTestRule.launchActivity(null);

        onView(withText(FROM))
                .check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldFillFieldFrom()
            throws Throwable {

        Place from = setUpFromMock();
        when(service.getCurrentPlace()).thenReturn(Maybe.empty());

        activityTestRule.launchActivity(null);

        onView(withId(R.id.origin_button))
                .perform(click());

        originTestSubject.onNext(from);

        onView(withText(FROM))
                .check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldFillFieldTo()
            throws Throwable {

        Place to = setupToMock();
        when(service.getCurrentPlace())
                .thenReturn(Maybe.empty());

        activityTestRule.launchActivity(null);

        onView(withId(R.id.destination_button))
                .perform(click());

        destinationTestSubject.onNext(to);

        onView(withText(TO)).check(matches(isDisplayed()));
    }

    @Test
    public void render_whenSelectedBothOriginAndDestinationShouldShowLoadingView() throws Throwable {

        Place from = setUpFromMock();
        Place to = setupToMock();

        PublishSubject<List<LatLng>> loading = PublishSubject.create();
        Maybe<List<LatLng>> source = Maybe.create(emitter ->
                loading.doOnComplete(emitter::onComplete));

        when(service.getCurrentPlace())
                .thenReturn(Maybe.empty());
        when(service.getDrivingDirections(any(), any()))
                .thenReturn(source);

        activityTestRule.launchActivity(null);

        onView(withId(R.id.origin_button))
                .perform(click());

        originTestSubject.onNext(from);

        onView(withId(R.id.destination_button))
                .perform(click());

        destinationTestSubject.onNext(to);

        onView(withId(R.id.selection))
                .check(matches(withEffectiveVisibility(GONE)));
        onView(withId(R.id.estimates))
                .check(matches(isDisplayed()));

        loading.onComplete();

    }

    @Test
    public void render_whenGettingDrivingDirectionsFailsShouldHideLoadingAndShowToast()
            throws Throwable {

        Place from = setUpFromMock();
        Place to = setupToMock();

        when(service.getCurrentPlace())
                .thenReturn(Maybe.empty());
        when(service.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.error(new Exception()));

        activityTestRule.launchActivity(null);

        onView(withId(R.id.origin_button)).perform(click());

        originTestSubject.onNext(from);

        onView(withId(R.id.destination_button)).perform(click());

        destinationTestSubject.onNext(to);

        onView(withId(R.id.estimates)).check(
                matches(withEffectiveVisibility(GONE)));
        onView(withId(R.id.selection))
                .check(matches(isDisplayed()));
        onView(withText(R.string.message_generic_error))
                .inRoot(ofToast())
                .check(matches(isDisplayed()));

        toaster.cancel();

    }

    @Test
    public void render_whenNoRouteIsFoundShouldShowToastAndExitEstimatesView()
            throws Throwable {

        Place from = setUpFromMock();
        Place to = setupToMock();

        when(service.getCurrentPlace())
                .thenReturn(Maybe.empty());
        when(service.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.error(new NoRouteFound()));

        activityTestRule.launchActivity(null);

        onView(withId(R.id.origin_button)).perform(click());

        originTestSubject.onNext(from);

        onView(withId(R.id.destination_button)).perform(click());

        destinationTestSubject.onNext(to);

        onView(withId(R.id.estimates)).check(
                matches(withEffectiveVisibility(GONE)));
        onView(withId(R.id.selection))
                .check(matches(isDisplayed()));
        onView(withText(R.string.message_no_route_error))
                .inRoot(ofToast())
                .check(matches(isDisplayed()));

        toaster.cancel();

    }

    @Test
    public void render_whenCloseButtonIsPressedShouldHideEstimatesView() {

        Place from = setUpFromMock();
        Place to = setupToMock();

        when(service.getCurrentPlace())
                .thenReturn(Maybe.just(from));
        when(service.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.never());

        activityTestRule.launchActivity(null);

        onView(withId(R.id.destination_button)).perform(click());

        destinationTestSubject.onNext(to);

        onView(withId(R.id.close_button)).perform(click());

        onView(withId(R.id.estimates)).check(matches(withEffectiveVisibility(GONE)));
        onView(withId(R.id.selection)).check(matches(isDisplayed()));

    }

    @Test
    public void render_whenErrorStateIsRenderedClearToAndWhenPickingANewFromShouldNotTriggerLoadingAgain() {

        Place from = setUpFromMock();
        Place to = setupToMock();

        when(service.getCurrentPlace())
                .thenReturn(Maybe.just(from));
        when(service.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.error(new Exception()));

        activityTestRule.launchActivity(null);

        onView(withId(R.id.destination_button)).perform(click());

        destinationTestSubject.onNext(to);

        onView(withId(R.id.origin_button))
                .check(matches(withText(FROM)));

        onView(withId(R.id.destination_button))
                .check(matches(withText("")));

        onView(withId(R.id.origin_button)).perform(click());

        originTestSubject.onNext(to);

        onView(withId(R.id.origin_button))
                .check(matches(withText(TO)));

        onView(withId(R.id.destination_button))
                .check(matches(withText("")));

    }

    @NonNull
    private Place setUpFromMock() {
        Place from = Mockito.mock(Place.class);
        when(from.getAddress()).thenReturn(FROM);
        when(from.getLatLng()).thenReturn(FROM_LAT_LON);
        return from;
    }

    @NonNull
    private Place setupToMock() {
        Place to = Mockito.mock(Place.class);
        when(to.getAddress()).thenReturn(TO);
        when(to.getLatLng()).thenReturn(TO_LAT_LON);
        return to;
    }


}