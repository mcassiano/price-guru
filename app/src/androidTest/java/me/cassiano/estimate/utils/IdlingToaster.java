package me.cassiano.estimate.utils;


import android.content.Context;
import android.view.View;
import android.widget.Toast;

import java.util.HashSet;

import static android.widget.Toast.LENGTH_SHORT;

public class IdlingToaster extends Toaster {

    private HashSet<Toast> toasts = new HashSet<>();

    @Override
    public Toast makeText(Context context, CharSequence text, int duration) {
        Toast toast = Toast.makeText(context, text, LENGTH_SHORT);
        toast.getView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View view) {
                toasts.add(toast);
            }

            @Override
            public void onViewDetachedFromWindow(View view) {
                toasts.remove(toast);
            }
        });
        return toast;
    }

    public void cancel() {
        for (Toast t : toasts)
            t.cancel();

        toasts.clear();
    }

}
