package me.cassiano.estimate.utils;

import android.os.Parcelable;

interface SafeParcelable extends Parcelable {
    String NULL = "SAFE_PARCELABLE_NULL_STRING";
    int SAFE_PARCEL_MAGIC = 20293;
}