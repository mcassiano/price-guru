package me.cassiano.estimate;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import org.mockito.Mockito;

import me.cassiano.estimate.data.service.CabifyApi;
import me.cassiano.estimate.data.service.PlaceService;
import me.cassiano.estimate.estimate.PickerAction;
import me.cassiano.estimate.utils.IdlingToaster;
import me.cassiano.estimate.utils.Toaster;

public class TestDependencyInjection extends DependencyInjection {

    private final PlaceService mockPlaceService = Mockito.mock(PlaceService.class);
    private final CabifyApi mockCabifyApi = Mockito.mock(CabifyApi.class);
    private final PickerAction originPickerAction = Mockito.mock(PickerAction.class);
    private final PickerAction destinationPickerAction = Mockito.mock(PickerAction.class);
    private final Toaster idlingToaster = new IdlingToaster();

    public TestDependencyInjection(Context context) {
        super(context);
    }

    @Override
    public PlaceService getPlaceService() {
        return mockPlaceService;
    }

    @Override
    public PickerAction pickOriginTransformer(AppCompatActivity activity) {
        return originPickerAction;
    }

    @Override
    public PickerAction pickDestinationTransformer(AppCompatActivity activity) {
        return destinationPickerAction;
    }

    @Override
    public CabifyApi getCabifyApi() {
        return mockCabifyApi;
    }

    @Override
    public Toaster getToaster() {
        return idlingToaster;
    }
}
