package me.cassiano.estimate.widget.estimates;

import android.support.test.runner.AndroidJUnit4;

import com.novoda.espresso.ViewTestRule;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.victoralbertos.device_animation_test_rule.DeviceAnimationTestRule;
import me.cassiano.estimate.data.model.local.Estimate;
import me.cassiano.estimate.data.model.local.ServiceEstimate;
import me.cassiano.estimate.data.presentation.model.PriceEstimatesViewState;
import me.cassiano.estimate.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.Visibility.GONE;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class PriceEstimatesTest {

    @ClassRule
    static public DeviceAnimationTestRule
            deviceAnimationTestRule = new DeviceAnimationTestRule();

    @Rule
    public final ViewTestRule<PriceEstimates> viewTestRule =
            new ViewTestRule<>(R.layout.test_price_estimates_view);


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void render_shouldShowLoadingViewAndCloseButton() {
        PriceEstimates view = viewTestRule.getView();
        PriceEstimatesViewState state = PriceEstimatesViewState
                .create(true, null);

        viewTestRule.runOnMainSynchronously(
                it -> view.render(state));

        onView(withId(R.id.dummy_content))
                .check(matches(isDisplayed()));

        onView(withId(R.id.close_button))
                .check(matches(isDisplayed()));

        onView(withId(R.id.view_title))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withId(R.id.estimate_list))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withId(R.id.no_coverage_image))
                .check(matches(withEffectiveVisibility(GONE)));

    }

    @Test
    public void render_shouldProperlyShowNoCoverageView() {
        PriceEstimates view = viewTestRule.getView();
        PriceEstimatesViewState state = PriceEstimatesViewState
                .create(false, ServiceEstimate
                        .builder()
                        .withinAreaOfOperation(false)
                        .build());

        viewTestRule.runOnMainSynchronously(
                it -> view.render(state));

        onView(withId(R.id.dummy_content))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withId(R.id.close_button))
                .check(matches(isDisplayed()));

        onView(withId(R.id.view_title))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withId(R.id.estimate_list))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withId(R.id.no_coverage_image))
                .check(matches(isDisplayed()));

    }

    @Test
    public void render_shouldProperlyShowItemsAndTitle() {
        PriceEstimates view = viewTestRule.getView();
        PriceEstimatesViewState state = PriceEstimatesViewState
                .create(false, ServiceEstimate
                        .builder()
                        .withinAreaOfOperation(true)
                        .addEstimate(Estimate
                                .builder()
                                .averageEta("8 min")
                                .className("Executive")
                                .classIconUrl("https://cabify.com/images/icons/vehicle_type/executive_54.png")
                                .priceFormatted("R$ 10,00")
                                .build())
                        .addEstimate(Estimate
                                .builder()
                                .averageEta("5 min")
                                .className("Lite")
                                .classIconUrl("https://cabify.com/images/icons/vehicle_type/executive_54.png")
                                .priceFormatted("R$ 8,00")
                                .build())
                        .build());

        viewTestRule.runOnMainSynchronously(
                it -> view.render(state));

        onView(withId(R.id.dummy_content))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withId(R.id.close_button))
                .check(matches(isDisplayed()));

        onView(withId(R.id.view_title))
                .check(matches(isDisplayed()));

        onView(withId(R.id.estimate_list))
                .check(matches(isDisplayed()));

        onView(withId(R.id.no_coverage_image))
                .check(matches(withEffectiveVisibility(GONE)));

        onView(withText("Executive")).
                check(matches(hasSibling(withText("8 min | R$ 10,00"))));

        onView(withText("Lite")).
                check(matches(hasSibling(withText("5 min | R$ 8,00"))));

    }

}