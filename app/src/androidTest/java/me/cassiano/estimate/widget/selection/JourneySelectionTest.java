package me.cassiano.estimate.widget.selection;

import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.novoda.espresso.ViewTestRule;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import io.victoralbertos.device_animation_test_rule.DeviceAnimationTestRule;
import me.cassiano.estimate.data.presentation.model.JourneySelectionState;
import me.cassiano.estimate.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static me.cassiano.estimate.data.presentation.model.JourneySelectionState.create;
import static org.hamcrest.Matchers.allOf;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
public class JourneySelectionTest {

    @ClassRule
    static public DeviceAnimationTestRule
            deviceAnimationTestRule = new DeviceAnimationTestRule();

    private static final String FROM = "R. Quintana, 549";
    private static final String TO = "R. Lisboa, 890";
    private static final String ALTERNATIVE = "R. George Ohm, 330";

    @Rule
    public final ViewTestRule<JourneySelection> viewTestRule =
            new ViewTestRule<>(R.layout.test_journey_selection_view);

    private final Place mockFrom = Mockito.mock(Place.class);
    private final Place mockTo = Mockito.mock(Place.class);
    private final Place mockAlternative = Mockito.mock(Place.class);
    private final Place mockMissingAddress = Mockito.mock(Place.class);

    @Before
    public void setUp() {
        when(mockFrom.getAddress()).thenReturn(FROM);
        when(mockTo.getAddress()).thenReturn(TO);
        when(mockAlternative.getAddress()).thenReturn(ALTERNATIVE);
        when(mockMissingAddress.getLatLng()).thenReturn(new LatLng(0, 1));
    }

    @Test
    public void render_shouldFillInFrom() {
        JourneySelection view = viewTestRule.getView();
        viewTestRule.runOnMainSynchronously(it ->
                view.render(create().withFrom(mockFrom)));

        onView(allOf(withId(R.id.origin_button),
                withText(FROM))).check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldFillInTo() {
        JourneySelection view = viewTestRule.getView();
        viewTestRule.runOnMainSynchronously(it ->
                view.render(create().withTo(mockTo)));

        onView(allOf(withId(R.id.destination_button),
                withText(TO))).check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldFillInFromAndTo() {
        JourneySelection view = viewTestRule.getView();
        viewTestRule.runOnMainSynchronously(it ->
                view.render(create(mockFrom, mockTo)));

        onView(allOf(withId(R.id.origin_button),
                withText(FROM))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.destination_button),
                withText(TO))).check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldProperlyChangeFrom() {
        JourneySelection view = viewTestRule.getView();
        JourneySelectionState original = create(mockFrom, mockTo);
        viewTestRule.runOnMainSynchronously(it ->
                view.render(original));

        onView(allOf(withId(R.id.origin_button),
                withText(FROM))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.destination_button),
                withText(TO))).check(matches(isDisplayed()));

        viewTestRule.runOnMainSynchronously(it ->
                view.render(original.withFrom(mockAlternative)));

        onView(allOf(withId(R.id.origin_button),
                withText(ALTERNATIVE))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.destination_button),
                withText(TO))).check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldProperlyChangeTo() {
        JourneySelection view = viewTestRule.getView();
        JourneySelectionState original = create(mockFrom, mockTo);
        viewTestRule.runOnMainSynchronously(it ->
                view.render(original));

        onView(allOf(withId(R.id.origin_button),
                withText(FROM))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.destination_button),
                withText(TO))).check(matches(isDisplayed()));

        viewTestRule.runOnMainSynchronously(it ->
                view.render(original.withTo(mockAlternative)));

        onView(allOf(withId(R.id.origin_button),
                withText(FROM))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.destination_button),
                withText(ALTERNATIVE))).check(matches(isDisplayed()));
    }

    @Test
    public void render_shouldProperlyRenderCoordinatesWhenMissingAddress() {
        JourneySelection view = viewTestRule.getView();
        JourneySelectionState state = create(mockMissingAddress, mockMissingAddress);

        viewTestRule.runOnMainSynchronously(it -> view.render(state));

        String coordinates = String.format("%f, %f", 0f, 1f);

        onView(allOf(withId(R.id.origin_button),
                withText(coordinates))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.destination_button),
                withText(coordinates))).check(matches(isDisplayed()));
    }

}