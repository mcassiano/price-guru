package me.cassiano.estimate.data.presentation;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import me.cassiano.estimate.data.EstimateRepository;
import me.cassiano.estimate.data.model.local.ServiceEstimate;
import me.cassiano.estimate.data.model.remote.cabify.EstimateResponse;
import me.cassiano.estimate.data.presentation.EstimateScreenViewModel.NoRouteFound;
import me.cassiano.estimate.data.presentation.model.EstimateScreenViewState;
import me.cassiano.estimate.data.service.PlaceService;
import me.cassiano.estimate.data.util.ResourceReader;

import static java.util.concurrent.TimeUnit.SECONDS;
import static me.cassiano.estimate.data.model.mapper.CabifyEstimateMapper.map;
import static me.cassiano.estimate.data.presentation.model.EstimateScreenViewState.create;
import static me.cassiano.estimate.data.util.MockFactory.FROM_LAT_LON;
import static me.cassiano.estimate.data.util.MockFactory.TO_LAT_LON;
import static me.cassiano.estimate.data.util.MockFactory.setUpFromMock;
import static me.cassiano.estimate.data.util.MockFactory.setupToMock;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EstimateScreenViewModelTest {

    private PublishSubject<Place> originIntent;
    private PublishSubject<Place> destinationIntent;
    private PublishSubject<Place> currentLocationIntent;
    private PublishSubject<Boolean> closeEstimatesIntent;
    private ReplaySubject<EstimateScreenViewState> renderedStates;
    private PlaceService placeService;
    private List<EstimateResponse> testFixture;
    private EstimateRepository estimateRepository;
    private EstimateScreenViewModel viewModel;
    private Place mockFrom;
    private Place mockTo;


    @Before
    public void setUp() throws Exception {

        RxJavaPlugins.setNewThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());

        JsonAdapter<List<EstimateResponse>> moshiAdapter = new Moshi.Builder()
                .build()
                .adapter(Types.newParameterizedType(List.class, EstimateResponse.class));

        testFixture = moshiAdapter
                .fromJson(ResourceReader.read("estimates_response.json"));
        originIntent = PublishSubject.create();
        destinationIntent = PublishSubject.create();
        currentLocationIntent = PublishSubject.create();
        closeEstimatesIntent = PublishSubject.create();
        placeService = Mockito.mock(PlaceService.class);
        estimateRepository = Mockito.mock(EstimateRepository.class);
        renderedStates = ReplaySubject.create();
        viewModel = new EstimateScreenViewModel(placeService, estimateRepository);
        viewModel.bindIntents(originIntent, destinationIntent,
                currentLocationIntent, closeEstimatesIntent);
        mockFrom = setUpFromMock();
        mockTo = setupToMock();
    }

    @After
    public void tearDown() throws Exception {
        viewModel.onCleared();
    }

    @Test
    public void observeState_shouldEmmitEmptyStateWhenSubscribed() throws Exception {

        List<EstimateScreenViewState> expected = Collections
                .singletonList(create());

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .blockingSubscribe(renderedStates);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitFromStateFromTheOriginIntent() throws Exception {

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(), create().withFrom(mockFrom));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitFromStateFromTheCurrentPlaceIntent() throws Exception {

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(), create().withFrom(mockFrom));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        currentLocationIntent.onNext(mockFrom);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitLoadingState() throws Exception {

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.error(new Exception()));

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(),
                create().withFrom(mockFrom),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withLoading(true));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);
        destinationIntent.onNext(mockTo);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitLoadingAndErrorState() throws Exception {

        Exception error = new Exception();

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.error(error));

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(),
                create().withFrom(mockFrom),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withLoading(true),
                create().withFrom(mockFrom)
                        .withError(error));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);
        destinationIntent.onNext(mockTo);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitDirectionsAndServiceEstimates() throws Exception {

        List<LatLng> directions = Arrays.asList(FROM_LAT_LON, TO_LAT_LON);
        ServiceEstimate serviceEstimate = map(testFixture);

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.just(directions));

        when(estimateRepository.getEstimates(mockFrom, mockTo))
                .thenReturn(Observable.just(serviceEstimate));

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(),
                create().withFrom(mockFrom),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withLoading(true),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withDirections(directions)
                        .withEstimates(serviceEstimate));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);
        destinationIntent.onNext(mockTo);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitEmptyDirectionsAndServiceEstimates() throws Exception {

        List<LatLng> directions = Collections.emptyList();
        NoRouteFound error = new NoRouteFound();

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.just(directions));

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(),
                create().withFrom(mockFrom),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withLoading(true),
                create().withFrom(mockFrom)
                        .withError(error));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);
        destinationIntent.onNext(mockTo);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitEmptyToAfterErrorAndPickingNewFromShouldNotLoadAgain() throws Exception {

        List<LatLng> directions = Collections.emptyList();
        NoRouteFound error = new NoRouteFound();

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.just(directions));

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.just(directions));

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(),
                create().withFrom(mockFrom),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withLoading(true),
                create().withFrom(mockFrom)
                        .withError(error),
                create().withFrom(mockTo),
                create().withFrom(mockTo)
                        .withTo(mockFrom)
                        .withLoading(true),
                create().withFrom(mockTo)
                        .withError(error));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);
        destinationIntent.onNext(mockTo);
        originIntent.onNext(mockTo);
        destinationIntent.onNext(mockFrom);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }

    @Test
    public void observeState_shouldEmmitClearToWhenCloseButtonIntentIsTriggered() throws Exception {

        when(placeService.getDrivingDirections(any(), any()))
                .thenReturn(Maybe.never());

        List<EstimateScreenViewState> expected = Arrays.asList(
                create(),
                create().withFrom(mockFrom),
                create().withFrom(mockFrom)
                        .withTo(mockTo)
                        .withLoading(true),
                create().withFrom(mockFrom));

        viewModel.observeState()
                .take(expected.size())
                .timeout(10, SECONDS)
                .subscribe(renderedStates);

        originIntent.onNext(mockFrom);
        destinationIntent.onNext(mockTo);

        closeEstimatesIntent.onNext(true);

        List<EstimateScreenViewState> rendered = renderedStates.toList().blockingGet();

        assertEquals(expected, rendered);

    }


}