package me.cassiano.estimate.data.model.mapper;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import me.cassiano.estimate.data.model.local.Estimate;
import me.cassiano.estimate.data.model.local.ServiceEstimate;
import me.cassiano.estimate.data.model.remote.cabify.EstimateResponse;
import me.cassiano.estimate.data.util.ResourceReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(BlockJUnit4ClassRunner.class)
public class CabifyEstimateMapperTest {

    private List<EstimateResponse> testFixture;

    @Before
    public void setup() throws IOException, URISyntaxException {

        JsonAdapter<List<EstimateResponse>> moshiAdapter = new Moshi.Builder()
                .build()
                .adapter(Types.newParameterizedType(List.class, EstimateResponse.class));

        testFixture = moshiAdapter
                .fromJson(ResourceReader.read("estimates_response.json"));
    }

    @Test
    public void map_getServiceShouldReturnCabify() throws Exception {
        List<EstimateResponse> results = Collections.emptyList();
        ServiceEstimate estimate = CabifyEstimateMapper.map(results);

        assertEquals("Cabify", estimate.getService());
    }

    @Test
    public void map_shouldMapEmptyListOfResultsToNoCoverageArea() throws Exception {
        List<EstimateResponse> results = Collections.emptyList();
        ServiceEstimate estimate = CabifyEstimateMapper.map(results);

        assertFalse(estimate.isWithinAreaOfOperation());

        estimate = CabifyEstimateMapper.map(null);

        assertFalse(estimate.isWithinAreaOfOperation());
    }

    @Test
    public void map_shouldMapCorrectNumberOfEstimates() throws IOException, URISyntaxException {

        ServiceEstimate estimates = CabifyEstimateMapper
                .map(testFixture);

        assertEquals(16, estimates.getEstimates().size());
    }

    @Test
    public void map_shouldMapToCorrectEntity() throws IOException, URISyntaxException {

        ServiceEstimate estimates = CabifyEstimateMapper
                .map(testFixture);

        EstimateResponse response = testFixture.get(0);
        Estimate estimate = estimates.getEstimates().get(0);

        assertEquals(response.getTotalPrice(), estimate.getTotalPrice());
        assertEquals(response.getCurrency(), estimate.getCurrency());
        assertEquals(response.getCurrencySymbol(), estimate.getCurrencySymbol());
        assertEquals(response.getPriceFormatted(), estimate.getPriceFormatted());
        assertEquals(response.getVehicleType().getDescription(), estimate.getClassDescription());
        assertEquals(response.getVehicleType().getEta().getFormatted(), estimate.getAverageEta());
        assertEquals(response.getVehicleType().getId(), estimate.getClassId());
        assertEquals(response.getVehicleType().getIcons().getRegular(), estimate.getClassIconUrl());
        assertEquals(response.getVehicleType().getShortName(), estimate.getClassName());
        assertTrue(estimate.isPriceAvailable());
    }

    @Test
    public void map_shouldSayPriceIsNotAvailableWhenTotalPriceIsNull() {

        Estimate estimate = CabifyEstimateMapper
                .map(testFixture)
                .getEstimates()
                .get(7);

        EstimateResponse response = testFixture.get(7);

        assertEquals(response.getVehicleType().getId(), estimate.getClassId());
        assertFalse(estimate.isPriceAvailable());
    }
}