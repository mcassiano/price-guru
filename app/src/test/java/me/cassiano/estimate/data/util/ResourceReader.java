package me.cassiano.estimate.data.util;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class ResourceReader {

    private ResourceReader() {
    }

    public static String read(String path) throws IOException, URISyntaxException {
        ClassLoader loader = ResourceReader.class.getClassLoader();
        URI resourceUri = loader.getResource(path).toURI();
        return Files.lines(Paths.get(resourceUri))
                .reduce((l1, l2) -> l1 + '\n' + l2)
                .orElse("");
    }

}
