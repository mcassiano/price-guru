package me.cassiano.estimate.data;

import org.junit.Before;
import org.junit.Test;

import io.reactivex.exceptions.CompositeException;
import me.cassiano.estimate.data.model.local.ServiceEstimate;
import me.cassiano.estimate.data.service.CabifyApi;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static me.cassiano.estimate.data.util.MockFactory.setUpFromMock;
import static me.cassiano.estimate.data.util.MockFactory.setupToMock;
import static me.cassiano.estimate.data.util.ResourceReader.read;
import static org.junit.Assert.assertEquals;


public class EstimateRepositoryTest {

    private EstimateRepository repository;
    private MockWebServer server;

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(server.url("/").toString())
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        repository = new EstimateRepository(retrofit.create(CabifyApi.class));
    }

    @Test
    public void getEstimates_shouldReturnResultsWhenSuccess() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(read("estimates_response.json")));

        ServiceEstimate estimates = repository
                .getEstimates(setUpFromMock(), setupToMock()).
                        blockingFirst();

        assertEquals(true, estimates.isWithinAreaOfOperation());
        assertEquals("Cabify", estimates.getService());
        assertEquals(16, estimates.getEstimates().size());

    }

    @Test
    public void getEstimates_shouldReturnNoCoverageWhenHttp404() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(404)
                .setBody(""));

        ServiceEstimate estimate = repository
                .getEstimates(setUpFromMock(), setupToMock())
                .blockingFirst();
        assertEquals(false, estimate.isWithinAreaOfOperation());
    }

    @Test(expected = CompositeException.class)
    public void getEstimates_shouldPropagateError() throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(401)
                .setBody(""));

        ServiceEstimate estimate = repository
                .getEstimates(setUpFromMock(), setupToMock())
                .blockingFirst();

    }

}