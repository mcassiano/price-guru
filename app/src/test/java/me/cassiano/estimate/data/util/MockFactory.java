package me.cassiano.estimate.data.util;


import android.support.annotation.NonNull;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class MockFactory {

    public static final String FROM = "R. Quintana, 549";
    public static final LatLng FROM_LAT_LON = new LatLng(-23.6043058, -46.6921347);
    public static final String TO = "R. Lisboa, 890";
    public static final LatLng TO_LAT_LON = new LatLng(-23.5569912, -46.6833528);

    private MockFactory() {
    }

    @NonNull
    public static Place setUpFromMock() {
        Place from = Mockito.mock(Place.class);
        when(from.getAddress()).thenReturn(FROM);
        when(from.getLatLng()).thenReturn(FROM_LAT_LON);
        when(from.getName()).thenReturn("");
        return from;
    }

    @NonNull
    public static Place setupToMock() {
        Place to = Mockito.mock(Place.class);
        when(to.getAddress()).thenReturn(TO);
        when(to.getLatLng()).thenReturn(TO_LAT_LON);
        when(to.getName()).thenReturn("");
        return to;
    }

}
